#include <Windows.h>
#include "..\GWIMGA.h"
#include "Detours.h"

HMODULE g_hModule;

class Log : public GWIMGA::Script {
	 
	ImGuiTextBuffer buffer;
	bool            ScrollToBottom;

	void OnRender(IDirect3DDevice9* device) override {
		{
			ImGui::SetNextWindowSizeConstraints(ImVec2(500, 400), ImVec2(1500, 1200));
			ImGui::Begin("[CtoS] Game Server");
			ImGui::TextUnformatted(buffer.begin());
			if (ScrollToBottom)
				ImGui::SetScrollHere(1.0f);
			ScrollToBottom = false;
			ImGui::End();
		}
	}
public:
	void  AddLog(const char* fmt, ...) IM_PRINTFARGS(2)
	{
		va_list args;
		va_start(args, fmt);
		buffer.appendv(fmt, args);
		va_end(args);
		ScrollToBottom = true;
	}
};

class TestWindowThingy : public GWIMGA::Script {

	void OnRender(IDirect3DDevice9*) override
	{
		{
			ImGui::ShowTestWindow();
		}
	}


};

Log* g__ctoslog;
void(__fastcall*ctos_tramp)(void*, DWORD, BYTE*) = nullptr;


void __fastcall sample_SendPacket(void* thisptr, DWORD size, BYTE* data)
{
	g__ctoslog->AddLog("<CtoGS> Sz: %X Header: %X",size,*(DWORD*)data);

	for (DWORD i = 0; i < size; ++i) {
		if (i % 16 == 0) g__ctoslog->AddLog("\n");
		g__ctoslog->AddLog("%02X ", data[i]);
	}

	g__ctoslog->AddLog("\n ------------------------------------------------ \n");

	ctos_tramp(thisptr, size, data);
}


BOOL WINAPI DllMain(HMODULE hModule, DWORD dwReason, LPVOID)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		g_hModule = hModule;
		GWIMGA::Initialize();
		g__ctoslog = new Log();
		ctos_tramp = (void(__fastcall*)(void*, DWORD, BYTE*))DetourFunc((BYTE*)0x58e130, (BYTE*)sample_SendPacket, 6);
		GWIMGA::AddScript(g__ctoslog);
		GWIMGA::AddScript(new TestWindowThingy());
		break;
	case DLL_PROCESS_DETACH:
		RetourFunc((BYTE*)0x58e130, (BYTE*)ctos_tramp, 6);
		GWIMGA::Destruct();
		break;
	}
	return TRUE;
}