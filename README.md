# GWIMGA - Guild Wars Immediate Mode GUI Api #

A simple ImGui implementation for Guild Wars. Can make quick loggers and other data retrieval tools with this api.

## How To Setup ##

### Quick Method ###
*Requires cl and vcvarsall to be setup on your PATH environment var. Google how to do this.*

1. Create a new folder for your project (this is the "root" folder from now on).
2. Use in Git:

```
git clone --recursive git@bitbucket.org:0x4D1/gwimga.git
```
3. Run build_release.bat in gwimga\ to create the library files for the projects.
4. Make your <name>.cpp in your root folder, create your app module (See How to Use (in code)).
5. Open a console window in the root directory.
6. Type these commands to build:
```
vcvarsall
gwimga\build_app <name>.cpp
```

### VS Project Method ###

1. Create a new vs project (vs2015 is what was used to develop, will have to change toolset otherwise)
2. Use in Git:

```
git clone --recursive git@bitbucket.org:0x4D1/gwimga.git
```

2. Add the GWIMGA.vcxproj and GWCA_DX.vcxproj project files to your solution.
3. Make sure GWCA_DX is referenced in GWIMGA (should be), then reference GWIMGA in your main project.
4. Write your main app (See How to Use (in code)) in your main project.
5. Build.

## How to Use (in code) ##

This "how to use" tutorial will go over how to make a program that creates a GUI window called "Guild Wars Rocks", with a label saying "Hello, World!", and a button that when clicked will make text appear.

First of all, you will be making a DLL project, so make sure that is specified if you are using a vs project.
Along with that, you will have to make a DllMain entry point. Here is a basic shell for any win32 dll.


```
#!c++

// Will need windows.h to get vital types and dependencies.
#include <Windows.h>


// Entry point.
BOOL WINAPI DllMain(HMODULE hModule,DWORD dwReason,LPVOID lpReserved)
{
	if(dwReason == DLL_PROCESS_ATTACH){
		// Code to execute on module load (init) here.
	}
	else if(dwReason == DLL_PROCESS_DETACH){
		// Code to execute on module unload (destruct/deinit) here.
	}
	return TRUE; // TRUE must always be sent, otherwise it default sends FALSE, which means that the module will be unloaded.
}

```

Given this basic shell, we can start adding gwimga.

The only include you have to include is GWIMGA.h

GWIMA's main system is 4 static functions, which handle initializing and destructing the library, as well as adding and removing "Scripts". These Scripts are modules that house your code for your gui and directx calls as well as whatever else you would like to encapsulate.

Basic top-level API Functions: (Note that PScript is just a pointer to a Script class, use will be explained later.)

```
#!c++
	// MUST BE CALLED BEFORE ANY SCRIPTS ARE MADE OR ADDED. Initialize the library.
	bool Initialize();

	// CLean up the library, call this on detach or close or whatever.
	void Destruct();

	// Call Initialize First. Add a script to the list to render.
	void AddScript(PScript script);

	// Remove said script above.
	void RemoveScript(PScript script);

```

Given this, we will add Initialize and Destruct to our DllMain:

```
#!c++

// Will need windows.h to get vital types and dependencies.
#include <Windows.h>
#include "gwimga\GWIMGA.h"

// Entry point.
BOOL WINAPI DllMain(HMODULE hModule,DWORD dwReason,LPVOID lpReserved)
{
	if(dwReason == DLL_PROCESS_ATTACH){
		if(!GWIMGA::Initialize()) return FALSE;
	}
	else if(dwReason == DLL_PROCESS_DETACH){
		GWIMGA::Destruct();
	}
	return TRUE; 
}
```
Now we shall add a script.

Scripts must be inherited from GWIMGA::Script, which houses multiple virtual functions to override.

Current virtuals:


```
#!c++

		// Called on first frame that the render function sees the script. Use to initialize hooks and stuff
		virtual bool OnInit(IDirect3DDevice9* device) { return true; }

		// Called on every frame. Use this to render shit and make your imgui shit
		virtual void OnRender(IDirect3DDevice9* device) { return; }

		// Not implemented atm but will be called on close to deinit what you do in init.
		virtual void OnDestruct(IDirect3DDevice9* device) { return; }

		// Called pre-dxreset to invalidate and clear your font buffers and shit. ImGui is handled for you.
		virtual void OnPreReset(IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* params) { return; }

		// Called pre-dxreset to reload and create the invalidated fonts. ImGui is handled for you.
		virtual void OnPostReset(IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* params) { return; }
```

In this tutorial, we will only need to use the "OnRender" virtual.

Using basic polymorphism designs that you should be familiar with, you can create the class as such:


```
#!c++
class MyTestScript : public GWIMGA::Script {

	void OnRender(IDirect3DDevice9*) override {
	
	}

};
```

And now we implement the GUI within the OnRender override. see the ImGui project for more detailed info on how to make guis.


```
#!c++

class MyTestScript : public GWIMGA::Script {

	void OnRender(IDirect3DDevice9*) override {
		static bool showbuttontext = false;
		ImGui::Begin("Guild Wars Rocks");
		ImGui::Text("Hello, World!");
		if(ImGui::Button("Clickme 4 Text")) showbuttontext ^= 1; // XOR operation used as a toggle.
		if(showbuttontext)
		{
			ImGui::Text("Text :D");
		}
		ImGui::End();
	}

};
```

Alright, now to add the script to the api at the start :D

This is simply done AFTER INITIALIZE IS CALLED by calling GWIMGA::AddScript(). In this case:


```
#!c++

GWIMGA::AddScript(new MyTestScript());
```

Alright, now were done in code :D, full source:


```
#!c++
#include <Windows.h>
#include "gwimga\GWIMGA.h"

class MyTestScript : public GWIMGA::Script {

	void OnRender(IDirect3DDevice9*) override {
		static bool showbuttontext = false;
		ImGui::Begin("Guild Wars Rocks");
		ImGui::Text("Hello, World!");
		if(ImGui::Button("Clickme 4 Text")) showbuttontext ^= 1; // XOR operation used as a toggle.
		if(showbuttontext)
		{
			ImGui::Text("Text :D");
		}
		ImGui::End();
	}

};



// Entry point.
BOOL WINAPI DllMain(HMODULE hModule,DWORD dwReason,LPVOID lpReserved)
{
	if(dwReason == DLL_PROCESS_ATTACH){
		if(!GWIMGA::Initialize()) return FALSE;
		GWIMGA::AddScript(new MyTestScript());
	}
	else if(dwReason == DLL_PROCESS_DETACH){
		GWIMGA::Destruct();
	}
	return TRUE; 
}

```

Quite simple :D, now compile using instructions below and inject into GW, and you should end up with this:

![Gw_2016-07-17_13-48-17.png](https://bitbucket.org/repo/GqRpAj/images/2694084644-Gw_2016-07-17_13-48-17.png)
![gw730.jpg](https://bitbucket.org/repo/GqRpAj/images/2344366875-gw730.jpg)

## TODO ##

* Make the imgui more generic for all dx9 games.
* Attempt to make the batch build process more intuitive
* Get OnDestruct working, along with exiting the dll within Imgui/Render Thread.