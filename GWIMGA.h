#pragma once

#include <Windows.h>
#include <imgui.h>

// GWIMGA - Guild Wars Immediate Mode Gui Api
// Created by 4D 1 @ gamerevision.com
// ImGui, the primary use of this library, created by https://github.com/ocornut

struct IDirect3DDevice9;
typedef struct _D3DPRESENT_PARAMETERS_ D3DPRESENT_PARAMETERS;

namespace GWIMGA {

	typedef class _SCRIPT {
	public:


		// Called on first frame that the render function sees the script. Use to initialize hooks and stuff
		virtual bool OnInit(IDirect3DDevice9* device) { return true; }

		// Called on every frame. Use this to render shit and make your imgui shit
		virtual void OnRender(IDirect3DDevice9* device) { return; }

		// Not implemented atm but will be called on close to deinit what you do in init.
		virtual void OnDestruct(IDirect3DDevice9* device) { return; }

		// Called pre-dxreset to invalidate and clear your font buffers and shit. ImGui is handled for you.
		virtual void OnPreReset(IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* params) { return; }

		// Called pre-dxreset to reload and create the invalidated fonts. ImGui is handled for you.
		virtual void OnPostReset(IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* params) { return; }

		// Dont worry about this, used for OnInit
		bool initialized() const { return initialized_; }
		void set_initialized() { initialized_ = true; }

	private:
		bool initialized_ = false;
	}Script,*PScript;


	// MUST BE CALLED BEFORE ANY SCRIPTS ARE MADE OR ADDED. Initialize the library.
	bool Initialize();

	// CLean up the library, call this on detach or close or whatever.
	void Destruct();

	// Call Initialize First. Add a script to the list to render.
	void AddScript(PScript script);

	// Remove said script above.
	void RemoveScript(PScript script);


}