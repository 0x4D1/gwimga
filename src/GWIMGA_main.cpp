#include "..\GWIMGA.h"

#include <vector>
#include <examples\directx9_example\imgui_impl_dx9.h>
#include <GWCA_DX\DirectXHooker.h>

/*		NAUGHTY NAUGHTY GLOBALS ;)		 */

HWND							g__GuildWarsWindow		= nullptr;

GWCA::DirectXHooker*			g__DxHooker				= nullptr;

GWCA::dx9::EndScene_t			g__OriginalEndScene		= nullptr;
GWCA::dx9::Reset_t				g__OriginalReset		= nullptr;
LONG							g__OriginalWndProc		= 0;

std::vector<GWIMGA::PScript>	g__Scripts				;

CRITICAL_SECTION				g__EndSceneSection;

/*		INTERNAL FUNCTION PROTOTYPES	 */

HRESULT WINAPI GWIMGA_EndScene(IDirect3DDevice9* device);
HRESULT WINAPI GWIMGA_Reset(IDirect3DDevice9* device,D3DPRESENT_PARAMETERS* params);
LRESULT WINAPI GWIMGA_WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

/*		EXPOSED FUNCTIONS/METHODS		 */

bool GWIMGA::Initialize()
{
	const BYTE hwnd_sig[] = { 0x85, 0xC0, 0x75, 0x17, 0x6A, 0x00, 0x56 };
	for (BYTE* scan = (BYTE*)0x401000; scan < (BYTE*)0x900000; ++scan){
		if (!memcmp(scan, hwnd_sig, sizeof(hwnd_sig))) {
			g__GuildWarsWindow = **(HWND**)(scan - 0x10);
			break;
		}		
	}

	if (g__GuildWarsWindow == nullptr) return false;

	if (!InitializeCriticalSectionAndSpinCount(&g__EndSceneSection,
		0x00000400))
		return false;

	g__DxHooker = new GWCA::DirectXHooker();

	g__OriginalEndScene = g__DxHooker->original<GWCA::dx9::EndScene_t>(GWCA::dx9::kEndScene);
	g__OriginalReset	= g__DxHooker->original<GWCA::dx9::Reset_t>(GWCA::dx9::kReset);

	ImGui_ImplDX9_Init(g__GuildWarsWindow, g__DxHooker->device());

	g__DxHooker->AddHook(GWCA::dx9::kEndScene, GWIMGA_EndScene);
	g__DxHooker->AddHook(GWCA::dx9::kReset, GWIMGA_Reset);

	g__OriginalWndProc = SetWindowLongPtr(g__GuildWarsWindow, GWLP_WNDPROC, (LONG)GWIMGA_WndProc);

	return true;
}


void GWIMGA::Destruct()
{
	EnterCriticalSection(&g__EndSceneSection);

	SetWindowLongPtr(g__GuildWarsWindow, GWLP_WNDPROC, g__OriginalWndProc);

	ImGui_ImplDX9_Shutdown();

	delete g__DxHooker;

	LeaveCriticalSection(&g__EndSceneSection);
	DeleteCriticalSection(&g__EndSceneSection);
}


void GWIMGA::AddScript(PScript script)
{
	g__Scripts.push_back(script);
}

void GWIMGA::RemoveScript(PScript script)
{
	for (auto it = g__Scripts.begin(); it != g__Scripts.cend(); ++it) {
		if (*it == script) {
			g__Scripts.erase(it);
		}
	}
}

/*		INTERNAL FUNCTIONS				 */

HRESULT WINAPI GWIMGA_EndScene(IDirect3DDevice9* device)
{

	if (TryEnterCriticalSection(&g__EndSceneSection))
	{
		ImGui_ImplDX9_NewFrame();

		for (auto it = g__Scripts.begin(); it != g__Scripts.cend(); ++it) {
			GWIMGA::PScript current = *it;
			if (current == nullptr) continue;
			if (!current->initialized()) {
				current->OnInit(device);
				current->set_initialized();
			}
			else {
				current->OnRender(device);
			}
		}

		ImGui::Render();

		LeaveCriticalSection(&g__EndSceneSection);
	}



	return g__OriginalEndScene(device);
}

HRESULT WINAPI GWIMGA_Reset(IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* params)
{
	for (auto it = g__Scripts.begin(); it != g__Scripts.cend(); ++it)
	{
		GWIMGA::PScript current = *it;
		if (current == nullptr) continue;
		current->OnPreReset(device, params);
	}


	ImGui_ImplDX9_InvalidateDeviceObjects();

	HRESULT result = g__OriginalReset(device, params);

	if (result == D3D_OK) {
		ImGui_ImplDX9_CreateDeviceObjects();

		for (auto it = g__Scripts.begin(); it != g__Scripts.cend(); ++it)
		{
			GWIMGA::PScript current = *it;
			if (current == nullptr) continue;
			current->OnPostReset(device, params);
		}
			

	}

	return result;
}

extern IMGUI_API LRESULT   ImGui_ImplDX9_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

LRESULT WINAPI GWIMGA_WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	ImGui_ImplDX9_WndProcHandler(hWnd, uMsg, wParam, lParam);

	auto& io = ImGui::GetIO();

	switch (uMsg) {
	case WM_RBUTTONDOWN: 
	case WM_RBUTTONUP:
	case WM_LBUTTONUP:
	case WM_MOUSEMOVE:
	case WM_LBUTTONDOWN:
	case WM_LBUTTONDBLCLK:
	case WM_MOUSEWHEEL:
		if (io.WantCaptureMouse)
			return true;
		break;

	case WM_KEYDOWN:
	case WM_SYSKEYDOWN:
	case WM_KEYUP:
	case WM_SYSKEYUP:
	case WM_CHAR:
	case WM_SYSCHAR:
	case WM_IME_CHAR:
	case WM_XBUTTONDOWN:
	case WM_XBUTTONUP:
	case WM_MBUTTONDOWN:
	case WM_MBUTTONUP:
		if (io.WantTextInput)
			return true;
		break;
	}

	return CallWindowProc((WNDPROC)g__OriginalWndProc, hWnd, uMsg, wParam, lParam);
}